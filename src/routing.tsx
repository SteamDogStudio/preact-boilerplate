import { Component, h } from 'preact';
import AsyncRoute from 'preact-async-route';
import { Router } from 'preact-router';

import { api } from './api';
import NotFound from './route-files/error';
import Loading from './route-files/loading';
import LoadingFailed from './route-files/loadingFailed';
import { baseRoutes } from './routes';

// Subpages
export class TLTRoutes extends Component<{}, {}> {
	private r = {
		ready: false,
		routes: null,
	};

	public changedRoute(e) {
		/*
		 * Route Change Event
		 */
		const inter = setInterval(() => {
			const cont: any = document.querySelector('#app');
			if (cont) {
				clearInterval(inter);
				cont.setAttribute('route', e.url);
			}
		}, 100);
	}

	private createRoute(route, path) {
		let allowed = true;
		if ( route.auth === true ) {
			allowed = false;
			if ( api.isLoggedIn() ) {
				allowed = true;
			}
		}
		return allowed ?
			<AsyncRoute
				path={`${path}`}
				loading={() => <Loading />}
				getComponent={() => this.fetchPage(`pages/${route.handler}`)}
			/>
			: <AsyncRoute
				path={`${path}`}
				loading={() => <Loading />}
				getComponent={() => this.fetchPage(`route-files/notAuthed`)}
			/>;
	}

	private mapRoutes(routes, parentRoute = '') {
		return routes.map(route => {
			const elms = [];
			if ( route.handler ) elms.push(this.createRoute(route, `${parentRoute}/${route.path}`));
			if ( route.children ) elms.push(this.mapRoutes(route.children, `${parentRoute}/${route.path}`));
			return elms.map(r => r);
		});
	}

	private async createRoutes() {
		this.r.routes = await this.mapRoutes(baseRoutes);
		this.r.ready = true;
		this.forceUpdate();
	}

	public async componentWillMount() {
		this.createRoutes();
	}

	public render() {
		return <div class="router-container">
			{ this.r.ready ?
				<Router onChange={this.changedRoute}>
					{ this.r.routes }
					<NotFound matches={{ code: '404' }} default />
				</Router>
				: <Loading />
			}
		</div>;
	}

	/**
	 * Load a page asynchronously.
	 *
	 * @param page Page name to import
	 * @return The JSX of the imported page.
	 */
	private async fetchPage(page: string): Promise<Element> {
		try {
			const module = await import(`./${page}`);
			return module.default;
		} catch (e) {
			return LoadingFailed as any;
		}
	}
}

