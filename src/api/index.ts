import { Fetch } from './fetch';

class API {
	public host = ``;
	public socket = ``;

	public async boot() {
		// malm
	}

	/**
	 * This is required for route authentication.
	 */
	public isLoggedIn() {
		const jwt = localStorage.getItem('jwt');
		if (jwt) {
			return true;
		}
		return false; // Default to false
	}
}

export const api = new API();
