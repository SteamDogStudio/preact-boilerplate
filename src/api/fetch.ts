import { route } from 'preact-router';

import { api } from './';

class FetchConstructor {
	public async EXTERNAL(url: string): Promise<any> {
		const headers = {};
		let options = {
			headers,
		};
		const json = await fetch(`${url}`, options)
			.then(res => {
				if ( !res.ok ) {
					console.info('[Error]', res.status, res.statusText);
					return;
				}
				return res.json();
			});
		return json;
	}

	public async CURRENT(url: string): Promise<any> {
		const headers = new Headers();
		headers.append('authorization', `${localStorage.getItem('jwt')}`);
		headers.append('content-type', 'application/json');

		const json = await fetch(`${api.host}${url}`, {
			headers,
		})
			.then(res => {
				return res.json();
			})
			.then(d => {
				return d;
			});

		return json;
	}

	public async GET(url: string): Promise<any> {
		const headers = new Headers();
		headers.append('authorization', `${localStorage.getItem('jwt')}`);
		headers.append('content-type', 'application/json');

		const json = await fetch(`${api.host}${url}`, {
			headers,
		})
			.then(res => {
				return res.json();
			})
			.then(d => {
				return d;
			});

		return json;
	}

	public async POST(url: string, body?: object): Promise<void> {
		const headers = new Headers();
		headers.append('authorization', `${localStorage.getItem('jwt')}`);
		headers.append('content-type', 'application/json');
		return await fetch(`${api.host}${url}`, {
			body: body ? JSON.stringify(body) : null,
			headers,
			method: 'POST',
		}).then(json => {
			if (!json.ok) {
				console.info('[Error]', json.status, json.statusText);
			}
		});
	}

	public async FILE(url: string, body?: FormData): Promise<void> {
		const headers = new Headers();
		headers.append('authorization', `${localStorage.getItem('jwt')}`);

		return await fetch(`${api.host}${url}`, {
			body,
			headers,
			method: 'PATCH',
		}).then(json => {
			if (!json.ok) {
				console.info('[Error]', json.status, json.statusText);
			}
		});
	}

	public async PATCH(url: string, body?: object): Promise<void> {
		const headers = new Headers();
		headers.append('authorization', `${localStorage.getItem('jwt')}`);
		headers.append('content-type', 'application/json');

		return await fetch(`${api.host}${url}`, {
			body: JSON.stringify(body),
			headers,
			method: 'PATCH',
		}).then(json => {
			if (!json.ok) {
				console.info('[Error]', json.status, json.statusText);
			}
		});
	}

	public async DELETE(url: string): Promise<void> {
		const headers = new Headers();
		headers.append('authorization', `${localStorage.getItem('jwt')}`);
		headers.append('content-type', 'application/json');

		return await fetch(`${api.host}${url}`, {
			headers,
			method: 'DELETE',
		}).then(json => {
			if (!json.ok) {
				console.info('[Error]', json.status, json.statusText);
			}
		});
	}
}

export const Fetch = new FetchConstructor();
