import { Component, h } from 'preact';

export default class NotAuthed extends Component<{}, {}> {
	public render(): JSX.Element {
		return (
			<main class="not-authed-component">
				<h1>Error: 403</h1>
				The requested page is unavailable as you're not Logged in!
			</main>
		);
	}
}
