import { Component, h } from 'preact';

export default class Error extends Component<{
	matches: any;
}, {}> {
	private code = this.props.matches.code ? this.props.matches.code : '404';
	private errors = {
		401: `Unauthorized`,
		403: `You don't have permission to view this page.`,
		404: `Page not found.`,
		410: `Channel not found.`,
	};

	public render() {
		// tslint:disable:max-line-length
		return <main class="error-component">
			<p>{this.errors[this.code]}</p>
		</main>;
	}
}
