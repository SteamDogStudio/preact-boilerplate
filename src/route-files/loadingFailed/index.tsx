import { Component, h } from 'preact';

/**
 * Loading failure page.
 */
export default class LoadingFailed extends Component<{}, {}> {
	public render(): JSX.Element {
		return (
			<main class="loading-failed-component">
				<h3>Oh no!</h3>
				The page failed to load! Please check you connection and try again.
			</main>
		);
	}
}
