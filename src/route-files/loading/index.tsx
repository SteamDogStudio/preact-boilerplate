import { Component, h } from 'preact';

export default class Loading extends Component<{}, {}> {
	public render(): JSX.Element {
		return (
			<main class="loading-component">
				Loading...
			</main>
		);
	}
}
