import { Component, h } from 'preact';

export default class NotFound extends Component<{}, {}> {
	public render(): JSX.Element {
		// tslint:disable:max-line-length
		return (
			<main class="not-found-component">
				<h1>Error: 404</h1>
				Page not found!
			</main>
		);
	}
}
