import './lib/handlers';

import { Component, h } from 'preact';

import style from './Global.scss';
import { TLTRoutes } from './routing';

/**
 * Main component of the application.
 */
export default class App extends Component<{}, {}> {
	public render(): JSX.Element {
		return <div id="app" style={style}>
			<TLTRoutes />
		</div>;
	}
}
