declare const module: NodeModule;

declare module '*.json' {
	const content: any;
	export default content;
}

declare module '*.css' {
	const content: any;
	export default content;
}

declare module '*.scss' {
	const content: any;
	export default content;
}

declare module '*.svg' {
	const content: any;
	export default content;
}

declare module '*.png' {
	const content: any;
	export default content;
}

interface IRoute {
	/**
	 * Determines whether a route requires authentication.
	 * @type string
	 */
	auth?: boolean;
	/**
	 * Child routes attached to the current route.
	 * @param `auth?` boolean;
	 * @param `children?` IRoute[];
	 * @param `guards?` string[];
	 * @param `handler?` string;
	 * @param `path` string;
	 */
	children?: IRoute[];
	/**
	 * Guards this route requires a user to have to access.
	 * @type string[]
	 */
	guards?: string[];
	/**
	 * The page to load when the route is hit.
	 * @type string
	 */
	handler?: string;
	/**
	 * The route path.
	 * @type string
	 */
	path: string;
}
