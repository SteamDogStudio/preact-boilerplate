import { h, render } from 'preact';

import { api } from './api';
import App from './App';

/**
 * Start the application.
 */
const start = async () => {
	await api.boot();
	render(<App />, document.body);
};

/**
 * Enable hot reloading for development
 */
if (module.hot) {
	module.hot.accept('./App', () => requestAnimationFrame(start));
}

start();
