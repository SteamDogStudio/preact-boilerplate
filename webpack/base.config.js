const { boilerpack } = require('boilerpack');
const { resolve } = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = boilerpack({
	devServer: {
		watchOptions: {
			poll: true,
		},
		historyApiFallback: {
			index: '/index.html',
		},
		port: 8080,
		host: '0.0.0.0',
		disableHostCheck: true,
		compress: true,
		noInfo: true,
		contentBase: resolve(__dirname, '../dist'),
	},
	devtool: 'source-map',

	optimization: {
		splitChunks: {
			chunks: 'all',
			maxInitialRequests: Infinity,
			minSize: 0,
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name(module) {
						// get the name. E.g. node_modules/packageName/not/this/part.js
						// or node_modules/packageName
						const packageName = module.context.match(
							/[\\/]node_modules[\\/](.*?)([\\/]|$)/,
						)[1];

						// npm package names are URL-safe, but some servers don't like @ symbols
						return `npm.${packageName.replace('@', '')}`;
					},
				},
			},
		},
	},
})
	.addEntry('main', ['./src/Main', 'preact'])
	.addExtensions('.ts', '.tsx', '.css', '.scss', '.json')
	.addRule('typescript', {
		test: /\.(tsx|ts)$/,
		use: 'awesome-typescript-loader',
	})
	.addRule('sass', {
		test: /\.s?css?$/,
		use: ['style-loader', 'css-loader', 'sass-loader'],
	})
	.addRule('inline-svg', {
		test: /\.inline\.svg$/i,
		use: ['preact-svg-loader'],
	})
	.addRule('image', {
		test: /\.(jpe?g|png|gif|svg)$/i,
		use: ['file-loader', 'image-webpack-loader'],
	})
	.addPlugin(
		new HtmlWebpackPlugin({
			template: resolve(__dirname, '../src/index.html'),
			filename: 'index.html',
			chunks: ['main'],
			minify: {
				removeComments: true,
				collapseWhitespace: true,
			},
		}),
	)
	.withOutput({
		publicPath: '/',
		filename: '[name].[hash].bundle.js',
		chunkFilename: '[chunkhash].chunk.js',
		path: resolve(__dirname, '../dist'),
	});
