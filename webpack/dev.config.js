const webpack = require('webpack');

const base = require('./base.config');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = base
	.addPlugins(
		new webpack.NamedModulesPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('development'),
			},
			DEBUG: true,
		}),
		// new BundleAnalyzerPlugin({
		// 	analyzerPort: 8881
		// }),
	)
	.make();
